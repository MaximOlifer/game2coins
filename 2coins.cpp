#include "2coins.h"
#include <QGridLayout>
#include <QMessageBox>
#include <QVBoxLayout>
#include <cstdlib>
#include <ctime>
#include <string>

Game2Coins::Game2Coins(QWidget *parent)
    : QWidget(parent) {

  plr1btn = new QPushButton("Игрок 1", this);
  plr2btn = new QPushButton("Игрок 2", this);
  accept = new QPushButton("Далее", this);
  QPushButton *resetbtn = new QPushButton("Сброс", this);
  score = new QLabel("Счет: 0/0", this);
  result1 = new QLabel("-", this);
  result2 = new QLabel("-", this);

  player1 = false;
  player2 = false;

  QVBoxLayout *vbox = new QVBoxLayout(this);
  QHBoxLayout *hbox1 = new QHBoxLayout();
  QHBoxLayout *hbox2 = new QHBoxLayout();

  vbox->addWidget(score);
  score->setAlignment(Qt::AlignCenter);
  hbox1->addWidget(result1);
  hbox1->addWidget(result2);
  result1->setAlignment(Qt::AlignCenter);
  result2->setAlignment(Qt::AlignCenter);
  vbox->addLayout(hbox1);
  hbox2->addWidget(plr1btn);
  hbox2->addSpacing(15);
  hbox2->addWidget(accept);
  hbox2->addSpacing(15);
  hbox2->addWidget(plr2btn);
  vbox->addLayout(hbox2);
  vbox->addWidget(resetbtn, 3, 0);

  setLayout(vbox);

  connect(plr1btn, &QPushButton::clicked, this, &Game2Coins::OnPlayer1Click);
  connect(plr2btn, &QPushButton::clicked, this, &Game2Coins::OnPlayer2Click);
  connect(resetbtn, &QPushButton::clicked, this, &Game2Coins::reset_score);
  connect(accept, &QPushButton::clicked, this, &Game2Coins::reset_buttons);

  std::srand(std::time(nullptr));

  accept->setEnabled(false);
}

// Обработка нажатия первого пользователя
void Game2Coins::OnPlayer1Click() {
  int randbit = rand() % 2;
  results_array[0] = randbit;
  player1 = true;

  result1->setText(tails_or_eagle(randbit).c_str());

  plr1btn->setEnabled(false);
  if (player1 && player2) {
    accept->setEnabled(true);
    update_score();
  }
}


// Обработка нажатия второго пользователя
void Game2Coins::OnPlayer2Click() {
    int randbit = rand() % 2;
    results_array[1] = randbit;
    player2 = true;

    result2->setText(tails_or_eagle(randbit).c_str());

    plr2btn->setEnabled(false);
    if (player1 && player2) {
      accept->setEnabled(true);
      update_score();
    }
}

// Обновление счета
void Game2Coins::update_score() {
    int result = results_array[0] != results_array[1];
    score_array[result] += 1;

    score->setText(("Счет: "+std::to_string(score_array[0]) + "/" + std::to_string(score_array[1])).c_str());
    if (score_array[result] >= 2) {
        std::string msg = "Игрок " + std::to_string(result+1) + " победил!\n"+"Счет: "+std::to_string(score_array[0]) + "/" + std::to_string(score_array[1]);
        QMessageBox::information(
            this,
            tr("2coins"),
            tr(msg.c_str())
        );
        reset_score();
    }
}

// Сброс
void Game2Coins::reset_score() {
    score_array[0] = 0;
    score_array[1] = 0;
    reset_buttons();
    score->setText(("Счет: "+std::to_string(score_array[0]) + "/" + std::to_string(score_array[1])).c_str());
}

// Обновление флажков, активация/дезактивации кнопок, обновление текста
void Game2Coins::reset_buttons() {
    player1 = false;
    player2 = false;
    plr1btn->setEnabled(true);
    plr2btn->setEnabled(true);
    accept->setEnabled(false);
    result1->setText("-");
    result2->setText("-");
}

// Строка "орел или решка" взамен числа 0 или 1
std::string Game2Coins::tails_or_eagle(int num) {
  if (num == 0) {
    return "Решка";
  }
  else {
    return "Орёл";
  }
}
