#pragma once

#include <QWidget>
#include <QApplication>
#include <QPushButton>
#include <QLabel>
#include <string>

class Game2Coins : public QWidget {

  Q_OBJECT // этот макрос должен включаться в классы, которые объявляют свои собственные сигналы и слоты

  public:
    Game2Coins(QWidget *parent = 0);

  private slots:
    void OnPlayer1Click();
    void OnPlayer2Click();
    void update_score();
    void reset_score();

  private:
    QLabel *score;
    QLabel *result1;
    QLabel *result2;

    QPushButton* plr1btn;
    QPushButton* plr2btn;
    QPushButton* accept;

    void reset_buttons();
    std::string tails_or_eagle(int);

    int score_array[2] = {0, 0};
    int results_array[2] = {0, 0};
    bool player1;
    bool player2;
};
